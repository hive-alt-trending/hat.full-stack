#!/bin/bash

if [ "$1" == "start" ]
then    
    chmod +x ./services/web/entrypoint.sh
    chmod +x ./services/web/entrypoint.prod.sh
    chmod +x ./services/streamer/entrypoint.sh
    chmod +x ./services/streamer/entrypoint.prod.sh
    if [ "$2" == "dev" ]
    then
      docker-compose down -v
      docker-compose up --build -d
    else
      docker-compose -f docker-compose.prod.yml down -v
      docker-compose -f docker-compose.prod.yml up --build -d
    fi

    sleep 5
    
    docker-compose exec -T web python manage.py create_db
fi

if [ "$1" == "restart" ]
then
  if [ "$2" == "dev" ]
  then
    docker-compose up --build -d
  else
    docker-compose -f docker-compose.prod.yml up --build -d
  fi
fi

if [ "$1" == "stop" ]
then
    docker-compose down -v --remove-orphans
fi

if [ "$1" == "db" ]
then
    winpty docker-compose exec db psql --username=hat_ai --dbname=hat_ai_prod || docker-compose exec db psql --username=hat_ai --dbname=hat_ai_prod
fi

if [ "$1" == "logs" ]
then
    docker-compose logs -f
fi