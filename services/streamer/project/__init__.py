from sqlalchemy.orm import sessionmaker
import ktrain
from beem import Hive
from beem.blockchain import Blockchain
from langdetect import detect
from bs4 import BeautifulSoup
import mistune
from . import models
from .database import engine


class Streamer():
    def __init__(self):
        self.predictor = ktrain.load_predictor('project/model')
        models.Post.metadata.create_all(engine, checkfirst=True)
        Session = sessionmaker(bind=engine)
        self.session = Session()
        hive = Hive(node='https://api.deathwing.me')
        self.blockchain = Blockchain(hive)
        self.stream()

    def stream(self):

        markdown = mistune.create_markdown(plugins=['table', 'strikethrough'])

        for post in self.blockchain.stream():

            if post["type"] != "comment":
                continue
            if post["parent_author"] != "":
                continue

            try:
                if detect(post["body"]) != "en":
                    continue

                author = post["author"]
                permlink = post["permlink"]
                title = post["title"]
                body = post["body"]
                timestamp = post["timestamp"]

                body = markdown(body)
                body = BeautifulSoup(body, "lxml").get_text()
                score = float(self.predictor.predict(body))

                new_post = models.Post(author=author, permlink=permlink, title=title, body=body, score=score, date=timestamp)
                self.session.add(new_post)
                self.session.commit()

            except Exception as e:
                print(e)
