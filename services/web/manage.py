import typer

from project import app, get_db
from project.models import Post

cli = typer.Typer()
db = get_db()

posts = []


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    for post in posts:
        db.session.add(Post(post['author'], post['body'], post['score'], post['title'], post['permlink']))
    db.session.commit()


if __name__ == "__main__":
    cli()
    app()
