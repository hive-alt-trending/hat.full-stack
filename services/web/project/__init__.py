from typing import List

from sqlalchemy.orm import Session
from fastapi.middleware.cors import CORSMiddleware
from fastapi import Depends, FastAPI

from . import models, schemas
from .database import SessionLocal, engine


models.Base.metadata.create_all(bind=engine)

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@app.api_route('/trending', methods=['GET'], response_model=List[schemas.Post])
async def trending(page: int = 1, pagesize: int = 50, db: Session = Depends(get_db)):
    page -= 1
    posts = db.query(models.Post) \
        .order_by(models.Post.score.desc()) \
        .limit(pagesize) \
        .offset(page*pagesize) \
        .all()
    print(posts)
    return posts
