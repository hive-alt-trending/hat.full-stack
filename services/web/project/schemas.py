from datetime import date
from pydantic import BaseModel


class Post(BaseModel):
    permlink: str
    title: str
    author: str
    body: str
    score: float
    date: date

    class Config:
        orm_mode = True