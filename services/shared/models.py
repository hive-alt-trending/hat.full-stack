from sqlalchemy import Column, Integer, String, Float, DateTime
from .database import Base
from sqlalchemy.sql import func


class Post(Base):
    __tablename__ = "post"

    id = Column(Integer(), primary_key=True)
    author = Column(String(20))
    permlink = Column(String(256))
    title = Column(String(256))
    body = Column(String(1024))
    date = Column(DateTime(timezone=True), server_default=func.now())
    score = Column(Float())

    def __init__(self, author, permlink, title, body, score, date):
        self.author = author
        self.title = title
        self.permlink = permlink
        self.body = body[:1024]
        self.score = score
        self.date = date
        