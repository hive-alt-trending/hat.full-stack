# Hive-Alt-Trending-Full-Stack

Docker container for the API, Database, blockchain streamer and webserver needed to run the hive alternate trending page. This code is standalone, meaning it can be run and spun up on any server with as little as two console commands.

## Useage
### Quickstart
- `sudo su` (linux)
- `chmod +x run.sh`, repeat for any necessary `entrypoint.sh` files (linux)
- `docker volume create --name=postgres_data`
- `./run.sh start` or `./run.sh start dev`
- Navigate to [localhost](http://localhost) in your browser (or [localhost:8000](http://localhost:8000) if started in dev mode).

*If run.sh fails or you are unable to run it, you must follow the following manual steps:*
- *`docker-compose -f docker-compose.prod.yml up --build -d` in the projects root directory*
- *`docker-compose exec web python manage.py create_db` then `docker-compose exec web python manage.py seed_db` to initialise and populate the DB*

### Accessing the DB

To ssh into the db container execute:
`./run.sh db`
or
`docker-compose exec db psql --username=hat_ai --dbname=hat_ai_prod`

Once in, `\c` to connect.

`\dt` to list tables.

Write regular SQL to view rows.

`\q` to quit.

### Rebuild and restart containers without deleting db changes (static files won't be updated)
`./run.sh restart` or `./run.sh restart dev`

### Stop containers and remove volumes (deletes db changes)
`./run.sh stop`

### View logs
`./run.sh logs`
